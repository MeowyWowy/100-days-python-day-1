import unittest
from unittest.mock import patch, call
from .switch_variables import switch_a_and_b
@patch('builtins.print')

class SwitchVariablesTest(unittest.TestCase):
    def test_switch_a_and_b(self, mock_print):
        switch_a_and_b('hello', 'goodbye')
        self.assertEqual(mock_print.mock_calls, [
            call("a: goodbye"),
            call("b: hello",)
        ])