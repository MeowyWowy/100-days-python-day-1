def switch_a_and_b(a = '', b = ''):
    if (len(a) == 0):
        a = input("a: ")
    if (len(b) == 0):
        b = input("b: ")

    original_a = a
    original_b = b
    a = original_b
    b = original_a

    print("a: " + a)
    print("b: " + b)

if __name__ == '__main__':
    switch_a_and_b()
