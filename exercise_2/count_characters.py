
def count_characters(input_string = ''):
    return len(input_string.strip())

if __name__ == '__main__':
    input_string = input('What is your name?')
    print(count_characters(input_string))