import unittest
from .count_characters import count_characters

class CountCharactersTest(unittest.TestCase):
    def test_no_space(self):
        self.assertEqual(count_characters('Danielle'), 8)
        self.assertEqual(count_characters('Sam'), 3)
        self.assertEqual(count_characters('Maggie'), 6)
    def test_with_space(self):
        self.assertEqual(count_characters('  Danielle '), 8)
    