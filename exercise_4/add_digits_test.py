import unittest
from .add_digits import add_digits

class AddDigitsTest(unittest.TestCase):
    def test_addition(self):
        self.assertEqual(add_digits('22'), 4)
        self.assertEqual(add_digits(29), 11)
    def test_non_numeric_input_fails(self):
        with self.assertRaises(Exception):
             add_digits('hi')

if __name__ == '__main__':
    unittest.main()