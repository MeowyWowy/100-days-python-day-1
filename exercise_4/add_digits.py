
def add_digits(number_input):
    if type(number_input) != str:
        number_input = str(number_input)
    if number_input.isdigit() == False:
        raise Exception('Input must be an integer')
    else:
        number_input = str(number_input)
        final_sum = 0
        for character in number_input:
            final_sum += int(character)
        return final_sum

if __name__ == '__main__':
    number_input = input("Type a two digit number: ")
    add_digits(number_input)
    
    