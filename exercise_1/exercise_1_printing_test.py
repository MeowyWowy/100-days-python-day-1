import unittest
from unittest.mock import patch, call
from .exercise_1_printing import exercise_1
@patch('builtins.print')

class PrintingTest(unittest.TestCase):
    def test_printing(self, mock_print):
        exercise_1()
        self.assertEqual(mock_print.mock_calls, [call("Day 1 - Python Print Function"), 
                                            call("The function is declared like this:"),
                                            call("print('what to print')"),])


if __name__ == '__main__':
    unittest.main()
