def exercise_1():
    print("Day 1 - Python Print Function")
    print("The function is declared like this:")
    print("print('what to print')")

if __name__ == '__main__':
    exercise_1()